@extends('layouts.master')
 



@section('contenido')





<div class="col-xs-12">

                    <h3 class="header smaller lighter">Control de vencimientos: 
                    <a href="{{URL::to('vencimiento/insert')}}"  class="btn btn-white btn-info btn-bold"> 
    <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Agregar</a>
    </h3>



                    <div class="clearfix">
                      <div class="pull-right tableTools-container"></div>
                    </div>
                    <div class="table-header">
                      Resultados
                    </div>
        
 
<table id="example" class="table table-striped table-bordered table-hover">
<div class="info"></div>
  <thead>
          <tr>
            <th>Tipo de documento</th>
            <th>Fecha de vencimiento</th>
            <th>Descripcion/Trabajador</th>
            <th>Dias restantes</th>
            <th>Acciones</th>
            
          </tr>
        </thead>

        <tfoot>
          <th></th>
          <th></th>
           <th></th>
          <th></th>
           <th></th>
   
  
        </tfoot>
        <tbody>


  @foreach($vencimientos as $vencimiento)
           <tr>

             <td>
              {{$vencimiento->categoria->nombre}}
            </td>
             <td>{{date_format(date_create($vencimiento->fecha_vencimiento),"d/m/Y")}}</td>
             <td>{{$vencimiento->descripcion}}</td>

             <?php

             $datetime1 = new DateTime($vencimiento->fecha_vencimiento);
             $datetime2 = new DateTime(date("Y/m/d"));
             $interval = $datetime1->diff($datetime2);
             if($interval->format("%R") == "+")
             {
               $dif = "<font color='red'>(". $interval->format('Venció hace %a')." Dias)</font>";
             }
             else
             {
               $dif = "<font color='green'>(". $interval->format('Faltan %a')." Dias)</font>";
             }

            

             ?>

              <td>{{ $dif }}
         

  <td class="td-actions">
                        
                      
                          <!--<a class="blue bootbox-mostrar" data-id={{$vencimiento->id}}>
                            <i class="fa fa-search-plus bigger-130"></i>
                          </a>
                          -->
                          <a data-toggle="modal" class="botoncito" data-urlarchivo="https://docs.google.com/viewer?url=http://35.182.137.28/qssam/public/archivos/vencimiento/{{$vencimiento->archivo}}&embedded=true"  href="#" >
                                <span class="label label-success arrowed">Vista Previa</span>
                              </a>
                        

                          <a class="green" href= {{ 'vencimiento/update/'.$vencimiento->id }}>
                            <i class="fa fa-pencil bigger-130"></i>
                          </a>

                         <a class="red bootbox-confirm" data-id={{ $vencimiento->id }}>
                            <i class="fa fa-trash bigger-130"></i>
                          </a>
                      </td>
</tr>
          @endforeach
        </tbody>
  </table>

  </div>






<!-- Categorías -->

<div class="col-xs-6">

                    <h3 class="header smaller lighter">Categorías: 
                    <a href="{{URL::to('categoriavencimiento/insert')}}"  class="btn btn-white btn-info btn-bold"> 
    <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Agregar</a>
    </h3>



                    <div class="clearfix">
                      <div class="pull-right tableTools-container"></div>
                    </div>
                    <div class="table-header">
                      Resultados
                    </div>
        
 
<table id="example1" class="table table-striped table-bordered table-hover">
  <thead>
          <tr>
            <th>Nombre</th>
          
  <th>Acciones</th>
            
          </tr>
        </thead>
        <tbody>


  @foreach($categorias as $categoria)
           <tr>

             <td> {{ $categoria->nombre}}</td>
         

  <td class="td-actions">
                       
                      


                          <a class="green" href= {{ 'categoriavencimiento/update/'.$categoria->id }}>
                            <i class="fa fa-pencil bigger-130"></i>
                          </a>

                         <a class="red bootbox-confirm2" data-id={{ $categoria->id }}>
                            <i class="fa fa-trash bigger-130"></i>
                          </a>
                      </td>
</tr>
          @endforeach
        </tbody>
  </table>

  </div>






      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Vista Previa</h4>
        </div>
        <div class="modal-body">

        
       
        
       <iframe id="iframe" src="" width="550" height="300" style="border: none;"></iframe> </div>
        
        <div class="modal-footer">
      
          
        </div>
      </div>
    </div>
  </div>



  <script type="text/javascript">
 $(document).ready(function() {


var table = $('#example').DataTable( {
      
      initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select style="width:60px"><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        },
       "language": {
                "url": "datatables.spanish.json"
            }
    } );





$( "#medicaactive" ).addClass( "active" );




$(".bootbox-confirm").on(ace.click_event, function() {
  var id = $(this).data('id');
var tr = $(this).parents('tr'); 

          bootbox.confirm("Deseas Eliminar el registro "+id, function(result) {
            if(result) { // si se seleccion OK
              
           
             
             $.get("{{ url('vencimiento/eliminar')}}",
              { id: id },

              function(data,status){ tr.fadeOut(1000); }
).fail(function(data){bootbox.alert("No se puede eliminar un registro padre: una restricción de clave externa falla");});

     
            }
           
          });
        });


$(".bootbox-confirm2").on(ace.click_event, function() {
  var id = $(this).data('id');
var tr = $(this).parents('tr'); 

          bootbox.confirm("Deseas Eliminar el registro "+id, function(result) {
            if(result) { // si se seleccion OK
              
           
             
             $.get("{{ url('categoriavencimiento/eliminar')}}",
              { id: id },

              function(data,status){ tr.fadeOut(1000); }
).fail(function(data){bootbox.alert("No se puede eliminar un registro padre: una restricción de clave externa falla");});

     
            }
           
          });
        });



$(".bootbox-mostrar").on(ace.click_event, function() {
  var id = $(this).data('id');

 $.get("{{ url('medica/mostrar')}}",
              { id: id },
              function(data)
              { 
                bootbox.dialog({message: data});

              });
          
             
         


     
            
           
          });
     



$(".botoncito").click(function(){
  var urlarchivo = $(this).data('urlarchivo');
  

  $("#urlarchivo").val(urlarchivo);


        $('#iframe').attr('src', urlarchivo);
        //$('#iframe').reload();

  $('#myModal').modal("show");
});



}); // fin ready
 </script>




        

        


@stop

