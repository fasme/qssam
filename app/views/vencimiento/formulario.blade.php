@extends('layouts.master')
 

 
@section('contenido')
     
<div class="page-header position-relative">
            <h1>
              Control de vencimiento
              <small>
                <i class="icon-double-angle-right"></i>
                
              </small>
            </h1>
          </div><!--/.page-header-->


     @if ($errors->any())
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <strong>Por favor corrige los siguentes errores:</strong>
      <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
      </ul>
    </div>
  @endif


<div class="row">
  <div class="col-xs-12">


<?php
  // si existe el usuario carga los datos
    if ($vencimiento->exists):
        $form_data = array('url' => 'vencimiento/update/'.$vencimiento->id, 'files'=>true);
        $action    = 'Editar';
    else:
        $form_data = array('url' => 'vencimiento/insert', 'class'=>'class="form-horizontal', 'files'=>true);
        $action    = 'Crear';        
    endif;

?>



{{ Form::open($form_data) }}
       
  

            <div class="form-group">
            {{Form::label('', 'Tipo de documento',array("class"=>"col-sm-3 control-label no-padding-right"))}}
            {{Form::select('categoria_vencimiento_id', $categorias, $vencimiento->categoria_vencimiento_id)}}
            </div>


             
            <div class="form-group">
            {{Form::label('', 'Fecha de vencimiento',array("class"=>"col-sm-3 control-label no-padding-right"))}}
            {{Form::text('fecha_vencimiento', date_format(date_create($vencimiento->fecha_vencimiento),'d/m/Y'), array("class"=>"date-picker", "id"=>"id-date-picker-1", "data-date-format"=>"dd/mm/yyyy"))}}
            </div>
            

             <div class="form-group">
            {{Form::label('', 'Descripcion/Trabajador',array("class"=>"col-sm-3 control-label no-padding-right"))}}
            {{Form::text('descripcion', $vencimiento->descripcion)}}
            
            </div>

             <div class="form-group">
            {{Form::label('Archivo', 'Archivo',array("class"=>"col-sm-3 control-label no-padding-right"))}}
            {{Form::file('archivo')}}
            {{$vencimiento->archivo}}
            </div>








           

           
        
     

             {{Form::submit('Guardar', array('class'=>'btn btn-small btn-success'))}}
        {{ Form::close() }}



    

  
</div>

   </div><!--/row-->



<script>
  $(document).ready(function(){
   

$( "#medicaactive" ).addClass( "active" );

   
$('.date-picker').datepicker({
          autoclose: true,
          todayHighlight: true
        });
    
  });   
</script>

@stop


